#include "document.h"

using namespace std;

ostream & operator<<(std::ostream & out, const Document & document)
{
	out << "{ "
		<< "document_id = " << document.id << ", "
		<< "relevance = " << document.relevance << ", "
		<< "rating = " << document.rating << " }";
	return out;
}

void PrintDocument(const Document & document)
{
	cout << "{ "
		<< "document_id = " << document.id << ", "
		<< "relevance = " << document.relevance << ", "
		<< "rating = " << document.rating << " }" << std::endl;
}

void PrintMatchDocumentResult(int document_id, const std::vector<std::string>& words, DocumentStatus status)
{
	cout << "{ "
		<< "document_id = " << document_id << ", "
		<< "status = " << static_cast<int>(status) << ", "
		<< "words =";
	for (const string& word : words) {
		cout << ' ' << word;
	}
	cout << "}" << std::endl;
}

void AddDocument(SearchServer & search_server, int document_id, const string & document, DocumentStatus status, const vector<int>& ratings) {
	try {
		search_server.AddDocument(document_id, document, status, ratings);
	}
	catch (const exception& e) {
		cout << "������ ���������� ��������� " << document_id << ": " << e.what() << endl;
	}
}

void FindTopDocuments(const SearchServer & search_server, const string & raw_query) {
	cout << "���������� ������ �� �������: " << raw_query << endl;
	try {
		for (const Document& document : search_server.FindTopDocuments(raw_query)) {
			PrintDocument(document);
		}
	}
	catch (const exception& e) {
		cout << "������ ������: " << e.what() << endl;
	}
}

void MatchDocuments(const SearchServer & search_server, const string & query) {
	try {
		cout << "������� ���������� �� �������: " << query << endl;
		const int document_count = search_server.GetDocumentCount();
		for (int index = 0; index < document_count; ++index) {
			vector<string> words;
			DocumentStatus status;
			const int document_id = search_server.GetDocumentId(index);
			tie(words, status) = search_server.MatchDocument(query, document_id);
			PrintMatchDocumentResult(document_id, words, status);
		}
	}
	catch (const exception& e) {
		cout << "������ �������� ���������� �� ������ " << query << ": " << e.what() << endl;
	}
}